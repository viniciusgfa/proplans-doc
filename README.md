
# Workflow Web Interface
🏠 This project is a hub focused on civil construction.

## Summary
- [Getting started](#💡Getting-started)

## 💡 Getting started
### Install
Choose one of the installation options based on your operating system::
>  - [Windows](./public/tutorial/install-with-wl2.md)
>  - [Windows - WSL2](./public/tutorial/install-with-wl2.md)
>  - [Linux](./public/tutorial/install-with-wl2.md)
>  - [macOS](./public/tutorial/install-with-wl2.md)

### Settings
#### Step 1 - Cloning the repository
[https://bitbucket.org/proplans/workflowwebinterface/src/master/](https://bitbucket.org/proplans/workflowwebinterface/src/master/)

#### Step 2 - Configuring the infrastructure
Inside the **infra** directory, edit the **docker-compose.yml** file by changing the following paths:
>
>- **/path/to/project/workflowwebinterface**: Here the root path of the project must be inserted.
>Ex:
>```yml
>  php73nginx:
>    ...
>    volumes:
>      - "/home/user/documents/workflowwebinterface:/var/www/html"
>    ...
>```
>
>- **/path/to/folder/mysql** : Here the path where mysql data will be stored must be inserted.
>Ex:
>```yml
>  mysql57:
>    ...
>    volumes:
>      - "/home/user/documents/workflowwebinterface/infra/mysql:/var/lib/mysql"
>    ...
>```
## ⭐ Features
- [x] User authentication
- [x] Project management
- [x] Contact management

## 🛠️ Technology
The following tools were used in the project:
- <a href="https://www.php.net/releases/7_3_0.php" target="_blank">PHP 7.3</a>
- <a href="https://www.nginx.com/" target="_blank">Nginx</a>
- <a href="https://getcomposer.org/" target="_blank">Composer</a>
- <a href="https://symfony.com/" target="_blank">Symfony</a>
- <a href="https://dev.mysql.com/doc/relnotes/mysql/5.7/en/" target="_blank">MySQL 5.7</a>

